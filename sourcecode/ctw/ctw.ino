
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <time.h>

#include "setting.h"
#include "control.h"
#include "processMQTT.h"
#include "configPortal.h"

Ticker ticker;

/***********************************************************
 * Timer xử lý đọc trạng thái nút nhấn và điều khiển led trạng thái
***********************************************************/
void tick() {
  
  static uint32_t lastTick = 0;

  if (pCTWData -> isInConfigPortal){
      pCTWData -> sysTick = millis();
      digitalWrite(LED_PIN, digitalRead(LED_PIN)^1);
    } else {
      if (!pCTWData -> successReadConfigFile){
          digitalWrite(LED_PIN, HIGH);
        }
      else {
        if (!pCTWData -> wifiConnected){
             if (pCTWData -> sysTick - lastTick> 2000){
                 lastTick = pCTWData -> sysTick ;
                 digitalWrite(LED_PIN, digitalRead(LED_PIN)^1);
              }
          }
        else if (!pCTWData -> MQTTConnected){
          
             if (pCTWData -> sysTick - lastTick> 500){
                 lastTick = pCTWData -> sysTick;
                 digitalWrite(LED_PIN, digitalRead(LED_PIN)^1);
              }
          }
        else {
            digitalWrite(LED_PIN, HIGH);
          }
        }
   }
  processControl(pCTWData);
}

/***********************************************************
 * Hàm setup, khởi tạo tham số
***********************************************************/
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(10);
  gpioInit();
  ticker.attach(0.05, tick);
  timeClient.begin();
  pCTWData -> successReadConfigFile = loadConfig(pCTWData);
  bool successConfigWifi = setupWifi();
  if (!successConfigWifi){
      startWifiManager(pCTWData);
    }
  client.setServer(pCTWData -> MQTTInfor.mqttServer.c_str(), pCTWData -> MQTTInfor.mqttPort);
  client.setCallback(callback);
  #ifdef DEBUG_FLAG
  Serial.println("Setup done, running loop!");
  #endif
}

/***********************************************************
 * Hàm loop, xử lý trạng thái kết nối và chế độ thiết lập
***********************************************************/
void loop() {
      pCTWData -> sysTick = millis();
      if (!pCTWData -> isInConfigPortal){
        pCTWData -> MQTTConnected = client.connected();
        pCTWData -> wifiConnected = WiFi.status() == WL_CONNECTED;
        client.loop();
        if (pCTWData -> MQTTConnected){
            timeClient.update();
            processMQTT(pCTWData);
          }
         else {
          if (pCTWData -> wifiConnected){
              reconnect(pCTWData);
            }
          }
        }
      checkConfigButton(pCTWData);
}
