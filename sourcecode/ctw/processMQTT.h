


#ifndef __PROCESSMQTT_H
#define __PROCESSMQTT_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "setting.h"
#include <NTPClient.h>
#include <WiFiUdp.h>

WiFiClient espClient;
PubSubClient client(espClient);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

/***********************************************************
 * Thiết lập wifi, chế độ station
***********************************************************/
bool setupWifi() {
  // We start by connecting to a WiFi network
  if (WiFi.SSID() == NULL)
    return false;
  WiFi.mode(WIFI_STA);
  #ifdef DEBUG_FLAG
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WiFi.SSID().c_str());
  #endif
  WiFi.setAutoReconnect(true);
  WiFi.begin();
  return true;
}
/***********************************************************
 * Callback nhận lệnh từ MQTT
***********************************************************/
void callback(char* topic, byte* payload, unsigned int length) {
  #ifdef DEBUG_FLAG
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  #endif
  char msg[length+1];
  for (int i = 0; i < length; i++) {
    msg[i] = (char)payload[i];
    #ifdef DEBUG_FLAG
    Serial.print((char)payload[i]);
    #endif
  }
  Serial.println();
  
  msg[length] = 0;

  if(strcmp(msg, "unlock") == 0){
      #ifdef DEBUG_FLAG
      Serial.println("Rev MQTT CMD");
      #endif
      pCTWData -> CTWMQTTControl.lockCmd = true;
      pCTWData -> CTWMQTTControl.lastTickRecieveMQTTCmd = pCTWData -> sysTick;
    }
}
/***********************************************************
 * Reconnect lại Broker khi mất kết nối
***********************************************************/
void reconnect(CTWDataTypeDefStruct * pCTWData) {
  // Loop until we're reconnected
  static uint32_t lastReconnect = 0;
  if ( (uint32_t)(pCTWData -> sysTick - lastReconnect) > 5000){
    if (!client.connected()) {
      #ifdef DEBUG_FLAG
      Serial.println("Attempting MQTT connection...");
      #endif
      // Attempt to connect
      if (client.connect(pCTWData -> MQTTInfor.mqttClientId.c_str(),pCTWData -> MQTTInfor.mqttUsername.c_str(),pCTWData -> MQTTInfor.mqttPassword.c_str(),
        pCTWData -> MQTTInfor.LWTTopic.c_str(),1,true,pCTWData -> MQTTInfor.LWTOfflineMsg,true)) {
        #ifdef DEBUG_FLAG
        Serial.println("connected");
        #endif
        // Once connected, publish an announcement...
        client.publish(pCTWData -> MQTTInfor.LWTTopic.c_str(), pCTWData -> MQTTInfor.LWTOnlineMsg,true);
        // ... and resubscribe
        client.subscribe(pCTWData -> MQTTInfor.cmdTopic.c_str());
      } else {
        #ifdef DEBUG_FLAG
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
        #endif
        lastReconnect = pCTWData -> sysTick;
      }
    }
  }
}
/***********************************************************
 * Publish trạng thái và confirm lệnh lên MQTT
***********************************************************/
void processMQTT(CTWDataTypeDefStruct * pCTWData){
    if (pCTWData -> needSendMQTT){
        if (pCTWData -> resultMessageToMQTT == unlockByPressButton){
            String msg = "{\"door\":\"unlocked\",\"timestamp\":" + (String)timeClient.getEpochTime() + "}";
            client.publish(pCTWData -> MQTTInfor.teleTopic.c_str(), msg.c_str());
            pCTWData -> needSendMQTT = false;
        }
        else if (pCTWData -> resultMessageToMQTT == lockByTimeOutPressButton){
            String msg = "{\"door\":\"locked\",\"timestamp\":" + (String)timeClient.getEpochTime() + "}";
            client.publish(pCTWData -> MQTTInfor.teleTopic.c_str(), msg.c_str());
            pCTWData -> needSendMQTT = false;
        }
        else if (pCTWData -> resultMessageToMQTT == unlockByMQTTCmd){
            String msg = "{\"door\":\"unlocked\",\"timestamp\":" + (String)timeClient.getEpochTime() + "}";
            client.publish(pCTWData -> MQTTInfor.resultTopic.c_str(), msg.c_str());
            pCTWData -> needSendMQTT = false;
        }
        else if (pCTWData -> resultMessageToMQTT == lockByTimeOutMQTTCmd){
            String msg = "{\"door\":\"locked\",\"timestamp\":" + (String)timeClient.getEpochTime() + "}";
            client.publish(pCTWData -> MQTTInfor.resultTopic.c_str(), msg.c_str());
            pCTWData -> needSendMQTT = false;
        }
        else if (pCTWData -> resultMessageToMQTT == nochangeByRepeatMQTTCmd){
            String msg = "{\"door\":\"nochange\",\"timestamp\":" + (String)timeClient.getEpochTime() + "}";
            client.publish(pCTWData -> MQTTInfor.resultTopic.c_str(), msg.c_str());
            pCTWData -> needSendMQTT = false;
        }
    }
}

#ifdef __cplusplus
}
#endif

#endif 
