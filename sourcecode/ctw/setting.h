#ifndef __SETTING_H
#define __SETTING_H
#ifdef __cplusplus
 extern "C" {
#endif

//Define for GPIO
#define CONTROL_BUTTON_PIN     14
#define LOCK_PIN               12
#define LED_PIN                4
#define CONFIG_BUTTON_PIN      5

//Define AP SSID and pass
#define SSID_AP         "CTW_NEWGYM"
#define PASSWORD_AP     "123456789"

//Undefine in Production mode
#define DEBUG_FLAG

typedef struct {
    bool     buttonIsPress = false;
    bool     buttonIsPressHolding = false;
    uint32_t firstTickPressButton = 0;
} CTWControlButtonTypeDefStruct;

typedef struct {
    bool     lockCmd = false;
    uint32_t lastTickRecieveMQTTCmd = 0;
} CTWControlMQTTTypeDefStruct;

typedef enum {
    locked,
    unlocked
} LockStatusTypeDefEnum;

typedef enum {
    noAction,
    unlockByPressButton,
    unlockByPressHoldingButton,
    lockByTimeOutPressButton,
    unlockByMQTTCmd,
    nochangeByRepeatMQTTCmd,
    lockByTimeOutMQTTCmd
} resultControlStatusTypeDefEnum;

typedef struct {
    String mqttServer   = "";
    int    mqttPort     = 1883;
    String mqttUsername = "";
    String mqttPassword = "";
    String mqttClientId = "";
    String LWTTopic     = "";
    const char * LWTOnlineMsg = "online";
    const char * LWTOfflineMsg = "offline";
    String cmdTopic    = "";
    String resultTopic = "";
    String teleTopic   = "";
    
  } MQTTConnectionInforTypeDefStruct;


typedef struct {
    uint32_t sysTick = 0;
    LockStatusTypeDefEnum         CTWLockStatus = locked;
    uint32_t firstTickUnLock = 0;
    
    MQTTConnectionInforTypeDefStruct  MQTTInfor;
    
    CTWControlButtonTypeDefStruct CTWButtonControl;
    CTWControlMQTTTypeDefStruct   CTWMQTTControl;
   
    resultControlStatusTypeDefEnum   resultControlNew    = noAction;
    resultControlStatusTypeDefEnum   resultControlOld    = noAction;
    resultControlStatusTypeDefEnum   resultMessageToMQTT = noAction;
    
    bool     isReadyForNewTypeControl = true;
    bool     needSendMQTT    = false;
    bool     wifiConnected  = false;
    bool     MQTTConnected  = false;
    bool     isInConfigPortal = false;
    
    bool     successReadConfigFile = false;
    
    int      unlockDelayButton = 5000;
    int      unlockDelayMQTT   = 5000;
    
} CTWDataTypeDefStruct;

CTWDataTypeDefStruct CTWData;
CTWDataTypeDefStruct * pCTWData = &CTWData;



#ifdef __cplusplus
}
#endif

#endif 
