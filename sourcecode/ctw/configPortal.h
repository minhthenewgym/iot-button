#include <ArduinoJson.h>
#include <FS.h>

#ifndef __CONFIGPORTAL
#define __CONFIGPORTAL
#ifdef __cplusplus
 extern "C" {
#endif

#include "setting.h"
 
/***********************************************************
 * Mở file lưu trữ giá trị thiết lập
 * File: config.txt
 * Kiểu dữ liệu: Json String
***********************************************************/
bool loadConfig(CTWDataTypeDefStruct * pCTWData){
  // Initialize SPIFFS
    if(!SPIFFS.begin()){
      #ifdef DEBUG_FLAG
      Serial.println("An Error has occurred while mounting SPIFFS");
      #endif
      return false;
    }
    File file = SPIFFS.open("/config.txt", "r");
    if (!file) {
      #ifdef DEBUG_FLAG
      Serial.println("Failed to open file for reading");
      #endif
      return false;
    }
    DynamicJsonDocument doc(512);
    DeserializationError error = deserializeJson(doc, file);
    if (error) {
      #ifdef DEBUG_FLAG
      Serial.println("Failed to read data");
      #endif
      file.close();
      return false;
    }
    #ifdef DEBUG_FLAG
    Serial.println("File Content: ");
    serializeJsonPretty(doc, Serial);
    #endif
    file.close();
  /***********************************************************
   * Cập nhật giá trị từ file thiết lập
  ***********************************************************/
    pCTWData -> MQTTInfor.mqttServer   = doc["mqttServer"].as<String>();
    pCTWData -> MQTTInfor.mqttPort     = doc["mqttPort"].as<int>();
    pCTWData -> MQTTInfor.mqttUsername = doc["mqttUsername"].as<String>();
    pCTWData -> MQTTInfor.mqttPassword = doc["mqttUsername"].as<String>();
    pCTWData -> MQTTInfor.mqttClientId = doc["mqttClientId"].as<String>();
    pCTWData -> MQTTInfor.LWTTopic     = "stat/" + pCTWData -> MQTTInfor.mqttClientId + "/status";
    pCTWData -> MQTTInfor.cmdTopic     = "cmd/"  + pCTWData -> MQTTInfor.mqttClientId;
    pCTWData -> MQTTInfor.resultTopic  = "stat/" + pCTWData -> MQTTInfor.mqttClientId + "/result";
    pCTWData -> MQTTInfor.teleTopic    = "tele/" + pCTWData -> MQTTInfor.mqttClientId;
    pCTWData -> unlockDelayMQTT        =  doc["unlockDelayMQTT"].as<int>();
    pCTWData -> unlockDelayButton      =  doc["unlockDelayButton"].as<int>();
    return true;
  }
  
bool shouldSaveConfig = false;
 //callback notifying us of the need to save config
void saveConfigCallback () {
  #ifdef DEBUG_FLAG
  Serial.println("Should save config");
  #endif
  shouldSaveConfig = true;
}
/***********************************************************
 * Thiết lập Portal để thiết lập Wifi và các thông số kết nối
***********************************************************/
void startWifiManager(CTWDataTypeDefStruct * pCTWData){
  #ifdef DEBUG_FLAG
  Serial.println("Start config portal");
  #endif
  pCTWData -> isInConfigPortal = true;
  WiFiManager wifiManager;
  
  wifiManager.setDebugOutput(false);
  
  WiFiManagerParameter custom_text1("<p><b>MQTT Settings</b></p>");
  WiFiManagerParameter custom_mqtt_server("mqttserver", "MQTT Server", pCTWData -> MQTTInfor.mqttServer.c_str() , 40);
  WiFiManagerParameter custom_mqtt_port("port", "Port", String(pCTWData -> MQTTInfor.mqttPort).c_str() , 6);
  WiFiManagerParameter custom_mqtt_username("mqttUsername", "Username", pCTWData -> MQTTInfor.mqttUsername.c_str(), 80);
  WiFiManagerParameter custom_mqtt_password("mqttPassword", "Password", pCTWData -> MQTTInfor.mqttPassword.c_str(), 80);
  WiFiManagerParameter custom_mqtt_clientId("mqttClientId", "Client Id", pCTWData -> MQTTInfor.mqttClientId.c_str(), 80);
  WiFiManagerParameter custom_text2("<p><b>Time Unlock Settings</b></p>");
  WiFiManagerParameter custom_mqtt_unlockDelayMQTT("unlockDelayMQTT", "MQTT CMD", String(pCTWData -> unlockDelayMQTT).c_str(), 10);
  WiFiManagerParameter custom_mqtt_unlockDelayButton("unlockDelayButton", "Button", String(pCTWData -> unlockDelayButton).c_str(), 10);
  
  wifiManager.addParameter(&custom_text1);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_username);
  wifiManager.addParameter(&custom_mqtt_password);
  wifiManager.addParameter(&custom_mqtt_clientId);
  wifiManager.addParameter(&custom_text2);
  wifiManager.addParameter(&custom_mqtt_unlockDelayButton);
  wifiManager.addParameter(&custom_mqtt_unlockDelayMQTT);
  
 //exit after config instead of connecting
  wifiManager.resetSettings();
  wifiManager.setBreakAfterConfig(true);
  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  if (!wifiManager.autoConnect(SSID_AP, PASSWORD_AP)) {
    #ifdef DEBUG_FLAG
    Serial.println("failed to connect, we should reset as see if it connects");
    #endif
    pCTWData -> isInConfigPortal = false;
    delay(3000);
    ESP.reset();
  }
  /***********************************************************
   * Lưu lại giá trị thiết lập
  ***********************************************************/
  if (shouldSaveConfig) {
    char setting_custom_mqtt_server[50];
    char setting_custom_mqtt_port[10];
    char setting_custom_mqtt_username[50];
    char setting_custom_mqtt_password[50];
    char setting_custom_mqtt_clientId[50];
    char setting_custom_mqtt_unlockDelayMQTT[10];
    char setting_custom_mqtt_unlockDelayButton[10];
    //read updated parameters
    strcpy(setting_custom_mqtt_server, custom_mqtt_server.getValue());
    strcpy(setting_custom_mqtt_port, custom_mqtt_port.getValue());
    strcpy(setting_custom_mqtt_username, custom_mqtt_username.getValue());
    strcpy(setting_custom_mqtt_password, custom_mqtt_password.getValue());
    strcpy(setting_custom_mqtt_clientId, custom_mqtt_clientId.getValue());
    strcpy(setting_custom_mqtt_unlockDelayMQTT, custom_mqtt_unlockDelayMQTT.getValue());
    strcpy(setting_custom_mqtt_unlockDelayButton, custom_mqtt_unlockDelayButton.getValue());
    StaticJsonDocument<512> doc;
    doc["mqttServer"] = setting_custom_mqtt_server;
    doc["mqttPort"] = atoi(setting_custom_mqtt_port);
    doc["mqttUsername"] = setting_custom_mqtt_username;
    doc["mqttPassword"] = setting_custom_mqtt_password;
    doc["mqttClientId"] = setting_custom_mqtt_clientId;
    doc["unlockDelayButton"] = atoi(setting_custom_mqtt_unlockDelayMQTT);
    doc["unlockDelayMQTT"] = atoi(setting_custom_mqtt_unlockDelayButton);
    #ifdef DEBUG_FLAG
    Serial.println("Config Data");
    serializeJsonPretty(doc, Serial);
    #endif
    File file = SPIFFS.open("/config.txt","w");
    if(!file){
      #ifdef DEBUG_FLAG
      Serial.println("Failed to open file for writing");
      #endif
    }
    if (serializeJson(doc, file) == 0) {
      #ifdef DEBUG_FLAG
      Serial.println("Failed to write to file");
      #endif
    }
    file.close();
   }
   pCTWData -> isInConfigPortal = false;
   delay(1000);
   ESP.reset();
 }
/***********************************************************
 * Kiểm tra nút nhấn thiết lập và vào chế độ thiết lập
***********************************************************/
void checkConfigButton(CTWDataTypeDefStruct * pCTWData){
  static uint32_t firstPressConfigButton = 0;
  if (!pCTWData -> isInConfigPortal && digitalRead(CONFIG_BUTTON_PIN) == 0){
    int32_t diffTime = (uint32_t)(pCTWData -> sysTick - firstPressConfigButton);
      if (diffTime > 5000){
        firstPressConfigButton = pCTWData -> sysTick;
        WiFi.disconnect(true);
        delay(1000);
        int counter = 0;
        while(WiFi.isConnected()){
          #ifdef DEBUG_FLAG
            Serial.println("Waiting Disconnect Wifi");
          #endif
            counter++;
            if (counter > 5)
              ESP.reset();
            delay(1000);
          }
        #ifdef DEBUG_FLAG
        Serial.println("Dissconected Wifi");
        #endif
        startWifiManager(pCTWData);
      }
   }
  else {
    firstPressConfigButton = pCTWData -> sysTick;
    }
  }
#ifdef __cplusplus
}
#endif
#endif 
