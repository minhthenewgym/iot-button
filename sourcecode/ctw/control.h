#ifndef __CONTROL_H
#define __CONTROL_H

#ifdef __cplusplus
 extern "C" {
#endif
#include "./setting.h"

/***********************************************************
 * Thiết lập GPIO
***********************************************************/
void gpioInit(void){
    pinMode(CONFIG_BUTTON_PIN, INPUT);
    pinMode(CONTROL_BUTTON_PIN, INPUT);
    pinMode(LOCK_PIN, OUTPUT);
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LOCK_PIN, LOW);
}
/***********************************************************
 * Kích ngắt khoá từ
***********************************************************/
void unclock(CTWDataTypeDefStruct * pCTWData){
    if (pCTWData -> CTWLockStatus != unlocked){
        pCTWData -> CTWLockStatus = unlocked;
        digitalWrite(LOCK_PIN, HIGH);
        pCTWData -> firstTickUnLock = pCTWData -> sysTick;
    }
}

/***********************************************************
 * Đóng khoá từ theo thời gian trễ
***********************************************************/

bool lock(CTWDataTypeDefStruct * pCTWData, uint32_t effectTime, uint32_t diffTime){
    if (effectTime > 0){
        if (diffTime > effectTime ){
            if (pCTWData -> CTWLockStatus != locked){
                pCTWData -> CTWLockStatus = locked;
                digitalWrite(LOCK_PIN, LOW);
                return true;
            }
            return true;
        }
        return false;
    } else {
        if (pCTWData -> CTWLockStatus != locked){
            pCTWData -> CTWLockStatus = locked;
            digitalWrite(LOCK_PIN, LOW);
            return true;
        }
    }
}
/***********************************************************
 * Kiểm tra nút nhấn có được nhấn hay không
 * TH1: nhấn nút
 * TH2: nhấn giữ nút
***********************************************************/
void checkPressButton(CTWDataTypeDefStruct * pCTWData){
    if (digitalRead(CONTROL_BUTTON_PIN) == 0){
        if (!pCTWData -> CTWButtonControl.buttonIsPress){
            pCTWData -> CTWButtonControl.buttonIsPress = true;
            pCTWData -> CTWButtonControl.firstTickPressButton = pCTWData -> sysTick;
        }
        else {
            if (!pCTWData -> CTWButtonControl.buttonIsPressHolding){
                uint32_t diffTime = (uint32_t)(pCTWData -> sysTick - pCTWData -> CTWButtonControl.firstTickPressButton);
                if (diffTime > pCTWData -> unlockDelayButton + 200){
                    pCTWData -> CTWButtonControl.buttonIsPressHolding = true;
                }
            }
        }
    } else {
        pCTWData -> CTWButtonControl.buttonIsPress = false;
        pCTWData -> CTWButtonControl.buttonIsPressHolding = false;
        pCTWData -> CTWButtonControl.firstTickPressButton = pCTWData -> sysTick;
    }
}
/***********************************************************
 * Gán trạng thái theo sự kiện điều khiển bởi nút nhấn
 * Tại một thời điểm chỉ có thể theo luồng sự kiện
 * điều khiển bằng nút nhấn hoặc MQTT
***********************************************************/
void checkControlByButton(CTWDataTypeDefStruct * pCTWData){
    checkPressButton(pCTWData);
    uint8_t isMQTTCmdProcess = pCTWData -> resultControlNew == unlockByMQTTCmd || pCTWData -> resultControlNew == nochangeByRepeatMQTTCmd || pCTWData -> resultControlNew == lockByTimeOutMQTTCmd;
    if (pCTWData->CTWButtonControl.buttonIsPress && !pCTWData->CTWButtonControl.buttonIsPressHolding){
        if (!isMQTTCmdProcess){
            pCTWData -> resultControlNew = unlockByPressButton;
        }
    } else if (pCTWData->CTWButtonControl.buttonIsPress && pCTWData->CTWButtonControl.buttonIsPressHolding){
        if (!isMQTTCmdProcess){
            pCTWData -> resultControlNew = unlockByPressHoldingButton;
        }
    } else {
        if (!isMQTTCmdProcess){
            if (pCTWData -> resultControlOld == unlockByPressButton || pCTWData -> resultControlNew == unlockByPressHoldingButton){
                pCTWData -> resultControlNew = lockByTimeOutPressButton;
            }
            else {
                pCTWData -> resultControlNew = noAction;
            }
        }
    }
}
/***********************************************************
 * Gán trạng thái theo sự kiện điều khiển bởi lệnh từ MQTT
 * Tại một thời điểm chỉ có thể theo luồng sự kiện
 * điều khiển bằng nút nhấn hoặc MQTT
***********************************************************/
void checkControlByMQTT(CTWDataTypeDefStruct * pCTWData){
    uint8_t isButtonCmdProcess = pCTWData -> resultControlNew == unlockByPressButton || pCTWData -> resultControlNew == unlockByPressHoldingButton || pCTWData -> resultControlNew == lockByTimeOutPressButton;
    if (pCTWData -> CTWMQTTControl.lockCmd){
        if (!isButtonCmdProcess){
            if (pCTWData -> CTWLockStatus   != unlocked){
                pCTWData -> resultControlNew = unlockByMQTTCmd;
            } else {
                pCTWData -> resultControlNew = nochangeByRepeatMQTTCmd;
            }
        }
        else {
            ;
        }
    }
    else {
        if (!isButtonCmdProcess){
            if (pCTWData -> resultControlOld == unlockByMQTTCmd || pCTWData -> resultControlNew == nochangeByRepeatMQTTCmd){
                pCTWData -> resultControlNew = lockByTimeOutMQTTCmd;
            } else {
                pCTWData -> resultControlNew = noAction;
            }
        }
    }
    pCTWData -> CTWMQTTControl.lockCmd = false;
}

/***********************************************************
 * Điều khiển và cập nhật trạng thái dựa theo các sự kiện được gán
***********************************************************/
void processControl(CTWDataTypeDefStruct * pCTWData){
    checkControlByMQTT(pCTWData);
    checkControlByButton(pCTWData);
    if (pCTWData -> resultControlOld != pCTWData -> resultControlNew){
        if (pCTWData -> resultControlNew == lockByTimeOutPressButton){
            if (pCTWData -> resultControlOld ==  unlockByPressButton){
                uint32_t diffTime = (uint32_t)(pCTWData -> sysTick - pCTWData -> firstTickUnLock);
                if(lock(pCTWData,pCTWData -> unlockDelayButton,diffTime)){
                    pCTWData -> resultControlOld = pCTWData -> resultControlNew;
                    #ifdef DEBUG_FLAG
                    Serial.println("lock by button time out");
                    #endif
                    pCTWData -> needSendMQTT = true;
                    pCTWData -> resultMessageToMQTT = lockByTimeOutPressButton;
                }
            } else if (pCTWData -> resultControlOld == unlockByPressHoldingButton) {
                lock(pCTWData,0,0);
                pCTWData -> resultControlOld = pCTWData -> resultControlNew;
                #ifdef DEBUG_FLAG
                Serial.println("lock by press holding button");
                #endif
                pCTWData -> needSendMQTT = true;
                pCTWData -> resultMessageToMQTT = lockByTimeOutPressButton;
            } else {
                lock(pCTWData,0,0);
                pCTWData -> needSendMQTT = true;
                pCTWData -> resultMessageToMQTT = lockByTimeOutPressButton;
            }
        } else if (pCTWData -> resultControlNew == unlockByPressButton){
            unclock(pCTWData);
            pCTWData -> resultControlOld = pCTWData -> resultControlNew;
            #ifdef DEBUG_FLAG
            Serial.println("unlock by press button");
            #endif
            pCTWData -> needSendMQTT = true;
            pCTWData -> resultMessageToMQTT = unlockByPressButton;
        } else if (pCTWData -> resultControlNew == unlockByPressHoldingButton){
            unclock(pCTWData);
            pCTWData -> resultControlOld = pCTWData -> resultControlNew;
            #ifdef DEBUG_FLAG
            Serial.println("unlock by pressHolding button");
            #endif
        } else if (pCTWData -> resultControlNew == unlockByMQTTCmd){
            #ifdef DEBUG_FLAG
            Serial.println("Unclock By MQTT");
            #endif
            unclock(pCTWData);

            pCTWData -> resultControlOld = pCTWData -> resultControlNew;
            pCTWData -> needSendMQTT = true;
            pCTWData -> resultMessageToMQTT = unlockByMQTTCmd;
            
        } else if (pCTWData -> resultControlNew == lockByTimeOutMQTTCmd){
            //uint32_t diffTime = (uint32_t)(pCTWData -> sysTick - pCTWData -> firstTickUnLock);
            uint32_t diffTime = (uint32_t)(pCTWData -> sysTick - pCTWData -> CTWMQTTControl.lastTickRecieveMQTTCmd);
            if(lock(pCTWData,pCTWData -> unlockDelayMQTT,diffTime)){
                pCTWData -> resultControlOld = pCTWData -> resultControlNew;
                #ifdef DEBUG_FLAG
                Serial.println("Lock By MQTT");
                #endif
                pCTWData -> needSendMQTT = true;
                pCTWData -> resultMessageToMQTT = lockByTimeOutMQTTCmd;
            }
        } else if (pCTWData -> resultControlNew == nochangeByRepeatMQTTCmd){
            #ifdef DEBUG_FLAG
            Serial.println("No change by repeat MQTT");
            #endif
            pCTWData -> needSendMQTT = true;
            pCTWData -> resultMessageToMQTT = nochangeByRepeatMQTTCmd;
        } else {
            pCTWData -> resultControlOld = pCTWData -> resultControlNew;
            #ifdef DEBUG_FLAG
            Serial.println("no action");
            #endif
        }
    }
}

#ifdef __cplusplus
}
#endif

#endif