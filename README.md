# CTW - New Gym

Công tắc điều khiển qua wifi và offline

## Thông tin về các file

```
    ctw-new-gym
        sourcecode -> thư mục chứ chương trình của thiết bị
            data
                config.txt -> file lưu trữ dữ liệu thiết lập cho thiết bị
            configPortal.h -> các hàm xử lý chế độ thiết lập, đọc và ghi file thiết lập
            control.h      -> các hàm đọc trạng thái và xử lý các sự kiện
            ctw.ino        -> chương trình chính, Arduino
            processMQTT.h  -> xử lý kết nối MQTT, dữ liệu nhận từ MQTT
            setting.h      -> khai báo cấu trúc dữ liệu, các thông số số mặc định cho thiết bị
        pcb       -> thư mục chứa các file thiết kế schematic và layout PCB
        images    -> thư mục lưu ảnh
        Mô tả thiết bị.pdf -> mô tả thiết bị
        README.md -> file markdown
```

## Mô tả sản phẩm, logic

```
    File: "Mô tả thiết bị.pdf"
```
## Topic

1. [User Manual](#user-manual)
2. [Hardware](#hardware)
3. [Firmware](#firmware)

# User Manual

[Quay về top](#topic)

### Sơ đồ đấu dây

<img src = "./images/wireDiagram.jpg" width="100%"/>

### Điều khiển

#### Điều khiển offline

- Nhấn nút nhấn: khi người dùng nhấn nút nhấn mở khoá, khoá từ sẽ kích ngắt sau khoảng thời gian `t1` giây thì được kích đóng trở lại.
- Nhấn giữ nút nhấn: khi người dùng nhấn giữ nút nhấn khoá từ sẽ kích ngắt đến khi người dùng nhả nút nhấn ra thì kích đóng.

Chú ý:
- Thời gian `t1` mặc định là 5 giây (5000ms), có thể thiết lập thông qua chế độ thiết lập.

#### Điều khiển online

Thiết bị nhận được lệnh kích ngắt khoá từ: Khoá từ sẽ kích ngắt sau khoảng thời gian `t2` giây thì được kích đóng trở lại.

Chú ý:
- Trong thời gian kích ngắt khoá từ, nếu như thiết bị tiếp tục nhận được lệnh kích ngắt khoá từ thì thời điểm kích đóng khoá từ sẽ được tính là khoảng thời gian tính từ thời điểm nhận lệnh điều khiển mới nhất.
- Thời gian `t2` mặc định là 5 giây (5000ms), có thể thiết lập thông qua chế độ thiết lập.

### Chế độ thiết lập
#### Thiết lập lần đầu:

Thiết bị tự phát wifi, chỉ cần thực hiện 2 bước:

- Bước 1: Kết nối tới Wifi mà thiết bị phát ra với tên Wifi và mật khẩu được thiết lập.
- Bước 2: Một Portal thiết lập được hiện ra tự động, người dùng tiến hành thiết lập các thông số và ấn lưu lại.

#### Thiết lập lại các thông số

- Bước 1: `Nhấn giữ` nút `thiết lập` khoảng 5 giây đến khi đèn led nhấp nháy liên tục với chu kì 0.05s thì buông tay.
- Bước 2: Kết nối tới Wifi mà thiết bị phát ra với tên Wifi và mật khẩu được thiết lập.
- Bước 3: Một Portal thiết lập được hiện ra tự động, người dùng tiến hành thiết lập các thông số và ấn lưu lại.
Lưu ý: Nếu Portal không tự hiện, người dùng mở trình duyệt và truy cập địa chỉ ``192.168.4.1`` để tiến hành thiết lập

### Led báo trạng thái

| Trạng thái Led              | Trạng thái thiết bị                |
|-----------------------------|------------------------------------|
| Không sáng                  | Bình thường                        |
| Sáng liên tục               | Không đọc được file thiết lập      |
| Sáng nhấp nháy chu kì 0.05s | Đang trong chế độ thiết lập        |
| Sáng nhấp nháy chu kì 0.5s  | Không kết nối được với MQTT Server |
| Sáng nhấp nháy chu kì 2s    | Không kết nối được với Wifi        |

Chú ý: Khi gặp lỗi `Không đọc được file thiết lập`, nguyên nhân có thể do người lập trình chưa phân vùng bộ nhớ hoặc vùng nhớ bị lỗi. Cần sử dụng tool [SPIFFS ESP8266](https://github.com/esp8266/arduino-esp8266fs-plugin) phân vùng lại bộ nhớ.

# Hardware
 
[Quay về top](#topic)

- File [CTW]pcb_v1.PcbDoc : File Layout PCB
- File [CTW]sch_v1.PcbDoc : File schematic
- Phần mềm thiết kế : Altium phiên bản 18.1.6

Schematic

<img src = "./images/Schematic.png" width="100%"/>

Layout PCB

<img src = "./images/layout3D.jpg" width="50%"/><img src = "./images/layout.jpg" width="50%"/>

## Firmware

[Quay về top](#topic)

### Framework và thư viện

Framework: [ESP8266 Arduino Core](https://github.com/esp8266/Arduino)

Chú ý chọn Board: `Generic ESP8266 Module` với các thông số thiết lập như hình:

<img src = "./images/selectBoard.png" width="50%"/>

### Thư viện chính:

[ArduinoJSON](https://github.com/bblanchon/ArduinoJson)

[PubsubClient](https://github.com/knolleary/pubsubclient)
 
[WiFiManager](https://github.com/tzapu/WiFiManager)

[NTPClient](https://github.com/arduino-libraries/NTPClient)

### Nạp chương trình

- Bước 1: Kết nối thiết bị với mạch Chuyển USB UART (có thể dùng CP2102, CH340,...)

    Sơ đồ kết nối

    | Thiết bị | USB UART |
    |----------|----------|
    | GND      | GND      |
    | TX       | RX       |
    | RX       | TX       |
    |          |          |
    |          |          |

    `Lưu ý: Không kết nối tín hiệu 3v3 khi đang cấp nguồn 12VDC cho thiết bị, có thể gây cháy mạch.`

- Bước 2: Gạt công tắc Boot Switch lên mức ON, ấn reset để vào boot mode.

- Bước 3: Nạp chương trình

- Bước 4: gạt công tắc Boot Switch xuống, ấn reset để vào chế độ chạy chương trình bình thường.